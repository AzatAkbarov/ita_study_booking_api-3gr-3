#!/usr/bin/env bash

php bin/console d:d:d --force
php bin/console d:d:c
php bin/console d:s:c
php bin/console doctrine:fixtures:load -n
php bin/console api:organization:register -o"Umbrella" -u"http://umbrella.com"
