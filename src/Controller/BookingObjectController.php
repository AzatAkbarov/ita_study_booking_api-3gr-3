<?php

namespace App\Controller;

use App\Entity\BookingObject;
use App\Model\BookingObject\BookingObjectHandler;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/booking")
 */
class BookingObjectController extends Controller
{
    /**
     * @Route("/get_all_objects", name="app_all_objects")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function getAllObjectsDataAction(BookingObjectRepository $bookingObjectRepository)
    {
        $allObjects = $bookingObjectRepository->findAll();
        if ($allObjects) {
            $array = [];
            foreach ($allObjects as $object) {
                $array[] = $object->__toArray();
            }
            return new JsonResponse($array);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/get_object_by_id/{id}", name="app_object_by_id")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param $id
     * @return JsonResponse
     */
    public function getObjectAction(BookingObjectRepository $bookingObjectRepository, $id)
    {
        $object = $bookingObjectRepository->find($id);
        if ($object) {
            $data = $object->__toArray();
            return new JsonResponse($data);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/get_filter_objects", name="app_filter_objects")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function getFilterObjectsDataAction(BookingObjectRepository $bookingObjectRepository, Request $request)
    {
        $data['word'] = $request->query->get('word');
        $data['from'] = $request->query->get('from');
        $data['before'] = $request->query->get('before');
        $data['type'] = $request->query->get('type');


        /**
         * @var BookingObject[] $filterObjects
         */
        $filterObjects = $bookingObjectRepository->findObjectsByCriteria($data);

        if ($filterObjects) {
            $array = [];
            foreach ($filterObjects as $object) {
                $array[] = $object->__toArray();
            }
            return new JsonResponse($array);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/create_new_booking_object", name="app_create_new_booking_object")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @param BookingObjectHandler $bookingObjectHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function createNewBookingObject(
        ClientRepository $clientRepository,
        ObjectManager $manager,
        BookingObjectHandler $bookingObjectHandler,
        Request $request
    )
    {
        $data['type'] = $request->request->get('type');
        $data['owner'] = $request->request->get('owner');
        $data['name'] = $request->request->get('name');
        $data['quantity'] = $request->request->get('quantity');
        $data['contact_person'] = $request->request->get('contact_person');
        $data['phone_number'] = $request->request->get('phone_number');
        $data['coordinates'] = $request->request->get('coordinates');
        $data['price'] = $request->request->get('price');


        if ($data['type'] == BookingObjectHandler::TYPE_PENSION) {
            $data['three_meals_a_day'] = $request->request->get('three_meals_a_day') ?? null;
            $data['mud_bath'] = $request->request->get('mud_bath') ?? null;
        } elseif ($data['type'] == BookingObjectHandler::TYPE_COTTAGE) {
            $data['cook'] = $request->request->get('cook') ?? null;
            $data['sauna'] = $request->request->get('sauna') ?? null;
        }

        $client = $clientRepository->findOneByPassport($data['owner']);

        if ($client) {
            $data['owner'] = $client;
        } else {
            return new JsonResponse(['error' => 'Ошибка!Что то пошло не так,Вас нету в базе и Вы не можете зарегистрировать объект'], 406);
        }

        try {
            $object = $bookingObjectHandler->createNewBookingObject($data);
            $manager->persist($object);
            $manager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Произошла ошибка!Объект не создался,попробуйте позже!'], 406);
        }

        return new JsonResponse(['result' => 'ok']);
    }
}