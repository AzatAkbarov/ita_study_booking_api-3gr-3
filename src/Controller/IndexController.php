<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_credentials/{passwordHash}/{email}", name="app_check_client")
     * @Method("HEAD")
     * @param string $passwordHash
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientAction(
        string $passwordHash,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($passwordHash, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_by_network_uid/{network}/{uid}", name="app_check_client_by_network_uid")
     * @Method("HEAD")
     * @param string $network
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientActionByNetworkUId(
        string $network,
        string $uid,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByUid($network, $uid)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/concrete_client_by_network_uid/{network}/{uid}", name="app_client_by_network_uid")
     * @Method("GET")
     * @param string $network
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByNetworkUIdAction(
        string $network,
        string $uid,
        ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByUid($network, $uid);
        if ($client) {
            return new JsonResponse($client->clientToArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client_by_email/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        /** @var Client $client || null */
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            return new JsonResponse($client->clientToArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['roles'] = $request->request->get('roles');
        $data['vkId'] = $request->request->get('vkId') ?? null;
        $data['faceBookId'] = $request->request->get('faceBookId') ?? null;
        $data['googleId'] = $request->request->get('googleId') ?? null;


        if (empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: ' . var_export($data, 1)], 406);
        }

        if ($clientRepository->findOneByPassportorEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'], 406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/client/edit/add_soc_binding", name="app_add_soc_binding")
     * @Method("PUT")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function addSocBindingClientAction(
        ClientRepository $clientRepository,
        ObjectManager $manager,
        Request $request
    )
    {
        $email = $request->request->get('email');
        $network = $request->request->get('network');
        $uid = $request->request->get('uid');

        if (empty($email) || empty($network) || empty($uid)) {
            return new JsonResponse(['error' => 'Недостаточно данных'], 406);
        }

        $client = $clientRepository->findOneByEmail($email);

        if ($client) {
            if (empty($client->getSocialId($network))) {
                try {
                    $client->setSocialId($network, $uid);
                    $manager->persist($client);
                    $manager->flush();
                } catch (\Exception $e) {
                    return new JsonResponse(['error' => 'Данная соц сеть уже используется другим аккаунтом'], 401);
                }
            } else {
                return new JsonResponse(['error' => 'Вы уже связывали свой аккаунт с данной соц сетью', 'status' => 'fail'], 406);
            }
        } else {
            throw new NotFoundHttpException();
        }
        return new JsonResponse(['result' => 'ok']);
    }
}

