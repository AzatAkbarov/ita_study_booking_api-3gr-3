<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Repository\BookingObjectRepository;
use App\Repository\BookingRepository;
use App\Repository\ClientRepository;
use DateInterval;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/online_booking")
 */
class BookingController extends Controller
{
    /**
     * @Route("/check_concrete_booking/{passport}", name="check_concrete_booking")
     * @Method("HEAD")
     * @param $passport
     * @param ClientRepository $clientRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function checkConcreteBookingAction($passport, ClientRepository $clientRepository, BookingRepository $bookingRepository)
    {
        $client = $clientRepository->findOneByPassport($passport);

        if ($bookingRepository->findOneByClientId($client->getId())) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/get_bookings_by_object/{id}", name="get_bookings")
     * @Method("GET")
     * @param BookingRepository $bookingRepository
     * @param BookingObjectRepository $bookingObjectRepository
     * @param $id
     * @return JsonResponse
     */
    public function getAllBookingsByObject(BookingRepository $bookingRepository,BookingObjectRepository $bookingObjectRepository, $id)
    {
        $object = $bookingObjectRepository->find($id);

        $roomsAmount = intval($object->getQuantity());
        $arrayOfBookings = [];
        for($i = 1;$i <= $roomsAmount; $i++) {
            $booking = $bookingRepository->findLastBookingByParams($id, $i);
            if($booking) {
                $arrayOfBookings[$i] = $booking->getReleaseDate()->format('d-m-Y');
            } else {
                $date =  new DateTime();
                $arrayOfBookings[$i] = $date->format('d-m-Y');
            }
        }
        return new JsonResponse($arrayOfBookings);
    }

    /**
     * @Route("/get_object_by_id/{id}", name="app_object_by_id")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param $id
     * @return JsonResponse
     */
    public function getObjectAction(BookingObjectRepository $bookingObjectRepository, $id)
    {
        $object = $bookingObjectRepository->find($id);
        if ($object) {
            $data = $object->__toArray();
            return new JsonResponse($data);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/create_booking", name="create_new_booking")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function createNewBookingObject(
        ClientRepository $clientRepository,
        ObjectManager $manager,
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        BookingRepository $bookingRepository
    )
    {
        $objectId = $request->request->get('objectId');
        $roomNumber = $request->request->get('roomNumber');
        $passport = $request->request->get('passport');


        $client = $clientRepository->findOneByPassport($passport);
        $object = $bookingObjectRepository->find($objectId);

        $booking = $bookingRepository->findLastBookingByParams($objectId, $roomNumber);

        if($booking) {
            $lastDate = $booking->getReleaseDate();
        } else {
            $lastDate = new DateTime();
        }
        $date = $lastDate->add(new DateInterval('P7D'));

        try {
            $booking = new Booking();
            $booking
                ->setBookingObject($object)
                ->setTenant($client)
                ->setRoomNumber($roomNumber)
                ->setReleaseDate($date);
            $manager->persist($booking);
            $manager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Произошла ошибка!Комната не забронирована,попробуйте позже!'], 406);
        }

        return new JsonResponse(['result' => 'ok']);
    }
}