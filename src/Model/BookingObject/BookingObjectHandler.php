<?php

namespace App\Model\BookingObject;


use App\Entity\Cottage;
use App\Entity\Pension;

class BookingObjectHandler
{
    const TYPE_COTTAGE = "cottage";
    const TYPE_PENSION = "pension";


    /**
     * @param array $data
     * @return mixed
     */
    public function createNewBookingObject(array $data) {
        $type = $data['type'];
        $object = null;
        switch ($type) {
            case self::TYPE_COTTAGE:
                $object = $this->createNewCottage($data);
                break;
            case self::TYPE_PENSION:
                $object = $this->createNewPension($data);
                break;
            default:
                $object = null;
        }
        $object
            ->setName($data['name'])
            ->setQuantity($data['quantity'])
            ->setContactPerson($data['contact_person'])
            ->setOwner($data['owner'])
            ->setPhoneNumber($data['phone_number'])
            ->setCoordinates($data['coordinates'])
            ->setPrice($data['price']);

        return $object;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createNewCottage(array $data) {
        /** @var Cottage $cottage */
        $cottage = new Cottage();
        $cottage
            ->setSauna($data['sauna'])
            ->setCook($data['cook']);
        return $cottage;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createNewPension(array $data) {
        /** @var Pension $pension */
        $pension = new Pension();
        $pension
            ->setMudBath($data['mud_bath'])
            ->setThreeMealsADay($data['three_meals_a_day']);
        return $pension;
    }

}