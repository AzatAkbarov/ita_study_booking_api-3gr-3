<?php

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return Client
     */
    public function createNewClient(array $data, bool $encodePassword = true ) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setRoles($data['roles']);
        if($encodePassword) {
            $password = $this->encodePassword($data['password']);
        } else {
            $password = $data['password'];
        }
        $client->setPassword($password);

        return $client;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePassword(string $password) {
        return md5($password).md5($password.'3');
    }


}