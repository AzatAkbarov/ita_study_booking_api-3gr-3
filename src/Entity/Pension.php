<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $three_meals_a_day;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $mud_bath;

    public function __construct()
    {
        parent::__construct();
        $this->objectName = 'Pension';
    }

    /**
     * @param $three_meals_a_day
     * @return Pension
     */
    public function setThreeMealsADay($three_meals_a_day) {

        $this->three_meals_a_day = $three_meals_a_day;
        return $this;
    }

    /**
     * @return bool
     */
    public function getThreeMealsADay() {

        return $this->three_meals_a_day;
    }

    /**
     * @param $mud_bath
     * @return Pension
     */
    public function setMudBath($mud_bath) {

        $this->mud_bath = $mud_bath;
        return $this;
    }

    /**
     * @return bool
     */
    public function getMudBath() {

        return $this->mud_bath;
    }

    /**
     * @return array
     */
    public function __toArray() {

        $arrayOfAllProperties =array_merge(parent::__toArray(), [
            'three_meals_a_day' => $this->three_meals_a_day,
            'mud_bath' => $this->mud_bath,
            'type' => get_class($this)
        ]);
        return $arrayOfAllProperties;
    }

}