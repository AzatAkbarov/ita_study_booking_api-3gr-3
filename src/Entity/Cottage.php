<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $cook;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $sauna;

    public function __construct()
    {
        parent::__construct();
        $this->objectName = 'Cottage';
    }

    /**
     * @param $cook
     * @return $this
     */
    public function setCook($cook) {

        $this->cook = $cook;
        return $this;
    }

    /**
     * @return string
     */
    public function getCook() {

        return $this->cook;
    }

    /**
     * @param $sauna
     * @return $this
     */
    public function setSauna($sauna) {

        $this->sauna = $sauna;
        return $this;
    }

    /**
     * @return string
     */
    public function getSauna() {

        return $this->sauna;
    }

    /**
     * @return array
     */
    public function __toArray() {

        $arrayOfAllProperties =array_merge(parent::__toArray(), [
            'cook' => $this->cook,
            'sauna' => $this->sauna,
            'type' => get_class($this)
        ]);
        return $arrayOfAllProperties;
    }

}