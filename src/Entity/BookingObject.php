<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"BookingObject" = "BookingObject", "Cottage" = "Cottage", "Pension" = "Pension"})
 */
class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $objectName;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $contact_person;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $phone_number;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="objects")
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $coordinates;

    /**
     * @ORM\Column(type="decimal",precision=10, scale=2)
     */
    private $price;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="bookingObject")
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    /**
     * @param int $id
     * @return BookingObject
     */
    public function setId(int $id): BookingObject
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return BookingObject
     */
    public function setName(string $name): BookingObject
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $quantity
     * @return BookingObject
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $contact_person
     * @return BookingObject
     */
    public function setContactPerson($contact_person)
    {
        $this->contact_person = $contact_person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contact_person;
    }

    /**
     * @param mixed $phone_number
     * @return BookingObject
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param Client
     * @return BookingObject
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return Client
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $coordinates
     * @return BookingObject
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param mixed $price
     * @return BookingObject
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function __toArray() {

        return [
            'id' => $this->getId(),
            'owner' => $this->getOwner(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'contact_person' => $this->getContactPerson(),
            'phone_number' => $this->getPhoneNumber(),
            'coordinates' => $this->getCoordinates(),
            'price' => $this->getPrice()
        ];
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return $this->objectName;
    }

    /**
     * @return ArrayCollection
     */
    public function getBookings(): ArrayCollection
    {
        return $this->bookings;
    }

    /**
     * @param Booking $booking
     */
    public function addBooking(Booking $booking)
    {
        $this->bookings->add($booking);
    }

    /**
     * @param Booking $booking
     */
    public function removeBooking(Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

}