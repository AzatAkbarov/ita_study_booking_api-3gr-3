<?php

namespace App\Entity;

use App\Model\Client\ClientHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $passport;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="BookingObject", mappedBy="owner")
     */
    private $objects;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $vkId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $faceBookId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $googleId;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="tenant")
     */
    private $bookings;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now");
        $this->objects = new ArrayCollection();
        $this->bookings = new ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return Client
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $vkId
     * @return Client
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * @param mixed $faceBookId
     * @return Client
     */
    public function setFaceBookId($faceBookId)
    {
        $this->faceBookId = $faceBookId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaceBookId()
    {
        return $this->faceBookId;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    /**
     * @param string $role
     */
    public function addRole(string $role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * @param mixed $googleId
     * @return Client
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    public function clientToArray() {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'roles' => $this->getRoles(),
            'passport' => $this->passport,
            'vkId' => $this->vkId,
            'faceBookId' => $this->faceBookId,
            'googleId' => $this->googleId,
        ];
    }

    public function setSocialId($network, $id) {
        switch($network) {
            case ClientHandler::SOC_NETWORK_VKONTAKTE:
                $this->vkId = $id;
                break;
            case ClientHandler::SOC_NETWORK_FACEBOOK:
                $this->faceBookId = $id;
                break;
            case ClientHandler::SOC_NETWORK_GOOGLE:
                $this->googleId = $id;
                break;
        }
        return $this;
    }

    public function getSocialId($network) {
        switch($network) {
            case ClientHandler::SOC_NETWORK_VKONTAKTE:
                return $this->vkId;
                break;
            case ClientHandler::SOC_NETWORK_FACEBOOK:
                return $this->faceBookId;
                break;
            case ClientHandler::SOC_NETWORK_GOOGLE:
                return $this->googleId;
                break;
            default:
                return null;
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * @param BookingObject $object
     */
    public function addObject(BookingObject $object)
    {
        $this->objects->add($object);
    }

    /**
     * @param BookingObject $object
     */
    public function removeObject(BookingObject $object)
    {
        $this->objects->removeElement($object);
    }

    /**
     * @return ArrayCollection
     */
    public function getBookings(): ArrayCollection
    {
        return $this->bookings;
    }

    /**
     * @param Booking $booking
     */
    public function addBooking(Booking $booking)
    {
        $this->bookings->add($booking);
    }

    /**
     * @param Booking $booking
     */
    public function removeBooking(Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }
}