<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var BookingObject
     * @ORM\ManyToOne(targetEntity="BookingObject", inversedBy="bookings")
     */
    private $bookingObject;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $releaseDate;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="bookings")
     */
    private $tenant;

    /**
     * @ORM\Column(type="integer")
     */
    private $roomNumber;


    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function __toArray() {

        return [
            'id' => $this->getId(),

        ];
    }

    /**
     * @param BookingObject $bookingObject
     * @return Booking
     */
    public function setBookingObject(BookingObject $bookingObject): Booking
    {
        $this->bookingObject = $bookingObject;
        return $this;
    }

    /**
     * @return BookingObject
     */
    public function getBookingObject(): BookingObject
    {
        return $this->bookingObject;
    }


    /**
     * @param mixed $roomNumber
     * @return Booking
     */
    public function setRoomNumber($roomNumber)
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    /**
     * @param Client $tenant
     * @return Booking
     */
    public function setTenant(Client $tenant): Booking
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Client
     */
    public function getTenant(): Client
    {
        return $this->tenant;
    }

    /**
     * @param \DateTime $releaseDate
     * @return Booking
     */
    public function setReleaseDate(\DateTime $releaseDate): Booking
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReleaseDate(): \DateTime
    {
        return $this->releaseDate;
    }


}