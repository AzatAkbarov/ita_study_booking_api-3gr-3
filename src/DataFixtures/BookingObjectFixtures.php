<?php

namespace App\DataFixtures;

use App\Model\BookingObject\BookingObjectHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingObjectFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var BookingObjectHandler
     */
    private $bookingObjectHandler;

    public const PENSION = 'Радуга';
    public const COTTAGE = '4 Сезона';

    public function __construct(BookingObjectHandler $bookingObjectHandler)
    {
        $this->bookingObjectHandler = $bookingObjectHandler;
    }

    public function load(ObjectManager $manager)
    {
        $pension1 = $this->bookingObjectHandler->createNewBookingObject([
            'type' => 'pension',
            'name' => 'Радуга',
            'quantity' => 12,
            'contact_person' => 'Акбаров Азат',
            'owner' => $this->getReference(ClientFixtures::CLIENT_TWO),
            'phone_number' => '0707-02-06-91',
            'coordinates' => '42.63229697425789,77.09823373087009',
            'price' => 300,
            'mud_bath' => true,
            'three_meals_a_day' => true
        ]);

        $manager->persist($pension1);

        $pension2 = $this->bookingObjectHandler->createNewBookingObject([
            'type' => 'pension',
            'name' => 'Иссык Куль',
            'quantity' => 15,
            'contact_person' => 'Петров Петр',
            'owner' => $this->getReference(ClientFixtures::CLIENT_TWO),
            'phone_number' => '0707-03-08-41',
            'coordinates' => '42.672437746448324,77.51384886165782',
            'price' => 400,
            'mud_bath' => true,
            'three_meals_a_day' => false
        ]);

        $manager->persist($pension2);

        $cottage1 = $this->bookingObjectHandler->createNewBookingObject([
            'type' => 'cottage',
            'name' => '4 Сезона',
            'quantity' => 6,
            'contact_person' => 'Дмитриев Дмитрий',
            'owner' => $this->getReference(ClientFixtures::CLIENT_THREE),
            'phone_number' => '0707-07-10-45',
            'coordinates' => '42.66670494811366,77.41674252268871',
            'price' => 666,
            'sauna' => true,
            'cook' => true
        ]);

        $manager->persist($cottage1);

        $cottage2 = $this->bookingObjectHandler->createNewBookingObject([
            'type' => 'cottage',
            'name' => 'У озера',
            'quantity' => 10,
            'contact_person' => 'Степанов Степан',
            'owner' => $this->getReference(ClientFixtures::CLIENT_THREE),
            'phone_number' => '0707-10-80-55',
            'coordinates' => '42.70395861716254,77.7507883287424',
            'price' => 1000,
            'sauna' => true,
            'cook' => false
        ]);

        $manager->persist($cottage2);

        $manager->flush();
        $this->addReference(self::PENSION, $pension1);
        $this->addReference(self::COTTAGE, $cottage1);
    }

    function getDependencies()
    {
        return array(
            ClientFixtures::class
        );
    }

}