<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Model\BookingObject\BookingObjectHandler;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $booking1 = new Booking();
        $before_date1 = new DateTime();

        $date1 = $before_date1->add(new DateInterval('P7D'));
        $booking1
            ->setBookingObject($this->getReference(BookingObjectFixtures::PENSION))
            ->setRoomNumber(2)
            ->setTenant($this->getReference(ClientFixtures::CLIENT_ONE))
            ->setReleaseDate($date1);

        $manager->persist($booking1);

        $booking2 = new Booking();
        $before_date2 = new DateTime();

        $date2 = $before_date2->add(new DateInterval('P7D'));
        $booking2
            ->setBookingObject($this->getReference(BookingObjectFixtures::COTTAGE))
            ->setRoomNumber(3)
            ->setTenant($this->getReference(ClientFixtures::CLIENT_FOUR))
            ->setReleaseDate($date2);

        $manager->persist($booking2);

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            ClientFixtures::class,
            BookingObjectFixtures::class
        );
    }

}