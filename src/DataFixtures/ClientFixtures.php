<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Model\Client\ClientHandler;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public const CLIENT_ONE = 'asd@asd.asd';
    public const CLIENT_TWO = 'qwe@qwe.qwe';
    public const CLIENT_THREE = 'zxc@zxc.zxc';
    public const CLIENT_FOUR = 'ewq@ewq.ewq';

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {

        $client1 = $this->clientHandler->createNewClient([
            'email' => 'asd@asd.asd',
            'passport' => 'AN12345678',
            'password' => '213471118',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client1);

        $client2 = $this->clientHandler->createNewClient([
            'email' => 'qwe@qwe.qwe',
            'passport' => 'AN87654321',
            'password' => '1123581321',
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client2);

        $client3 = $this->clientHandler->createNewClient([
            'email' => 'zxc@zxc.zxc',
            'passport' => 'AN12344321',
            'password' => '32571219',
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client3);

        $client4 = $this->clientHandler->createNewClient([
            'email' => 'ewq@ewq.ewq',
            'passport' => 'AN123',
            'password' => '123',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client4);

        $manager->flush();
        $this->addReference(self::CLIENT_ONE, $client1);
        $this->addReference(self::CLIENT_TWO, $client2);
        $this->addReference(self::CLIENT_THREE, $client3);
        $this->addReference(self::CLIENT_FOUR, $client4);
    }

}