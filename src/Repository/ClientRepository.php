<?php

namespace App\Repository;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return Client|null
     */
    public function findOneByPassportOrEmail(string $passport, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.passport = :passport')
                ->orWhere('c.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $password
     * @param string $email
     * @return Client|null
     */
    public function findOneByPasswordAndEmail(string $password, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.password = :password')
                ->andWhere('c.email = :email')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $email
     * @return Client|null
     */
    public function findOneByEmail(string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $passport
     * @return Client|null
     */
    public function findOneByPassport(string $passport) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.passport = :passport')
                ->setParameter('passport', $passport)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByUid(string $network, string $uid)
    {
        try {
            $qb = $this->createQueryBuilder('c')
                ->select('c');
            switch ($network) {
                case ClientHandler::SOC_NETWORK_VKONTAKTE:
                    $qb->where("c.vkId = :uid");
                    break;
                case ClientHandler::SOC_NETWORK_FACEBOOK:
                    $qb->where("c.faceBookId = :uid");
                    break;
                case ClientHandler::SOC_NETWORK_GOOGLE:
                    $qb->where("c.googleId = :uid");
                    break;
            }
            return $qb->setParameter('uid', $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}