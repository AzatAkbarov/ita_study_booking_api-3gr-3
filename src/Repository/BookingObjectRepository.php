<?php

namespace App\Repository;

use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findObjectsByCriteria($data)
    {
        $db = $this->createQueryBuilder('b')
            ->select('b')
            ->where('b.name LIKE :word')
            ->andWhere('b.price BETWEEN :from AND :before');
        if (!empty($data['type'])) {
            $db->andWhere('b.objectName = :type')
                ->setParameter(':type', $data['type']);
        }
        return $db->setParameter('word', "%" . $data['word'] . "%")
            ->setParameter(':from', $data['from'] ?? 0)
            ->setParameter(':before', $data['before'] ?? 1000000000)
            ->getQuery()
            ->getResult();
    }
}