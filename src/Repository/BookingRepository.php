<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param $clientId
     * @return Booking|null
     */
    public function findOneByClientId($clientId) : ?Booking
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.tenant = :clientId')
                ->setParameter('clientId', $clientId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findLastBookingByParams($objectId, $roomNumber) {
        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.bookingObject = :objectId')
                ->andWhere('b.roomNumber = :roomNumber')
                ->setParameter('objectId', $objectId)
                ->setParameter('roomNumber', $roomNumber)
                ->orderBy('b.releaseDate','DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}